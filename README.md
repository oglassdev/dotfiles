# oglass's dotfiles

these are my dotfiles and theres an installer so you can use that.

### Post-install Instructions
1. **NVidia**
   a. Add `nvidia nvidia_modeset nvidia_uvm nvidia_drm` to your mkinitcpio.conf, then run `sudo mkinitcpio -P`
   b. Add `nvidia_drm.modeset=1 nvidia.NVreg_PreserveVideoMemoryAllocations=1` to your kernel parameters
   c. Enable the services `nvidia-suspend.service`, `nvidia-hibernate.service`, and `nvidia-resume.service`
2. **Misc**
   a. Edit `/etc/greetd/config.toml`
