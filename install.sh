#!/bin/bash

# Install packages in a list
install_packages() {
    local packages=("$@")
    echo "Installing packages: ${packages[*]}"
    yay -S --needed --noconfirm "${packages[@]}" || error_exit "Failed to install packages: ${packages[*]}"
}

# Prompt the user for input
prompt() {
    local prompt="$1"
    while true; do
        read -rp "$prompt [Y/N]: " answer
	case $answer in
	    [Yy]*  ) return 0;;
	    [Nn]*  ) return 1;;
	    * ) printf "Invalid response!\n"
        esac
    done
}

# Packages
NVIDIA_DKMS_PACKAGES=(
    nvidia-dkms
    nvidia-utils
    nvidia-settings
    lib32-nvidia-utils
)
DEFAULT_PACKAGES=(
    hyprland-git
    waybar
    swww
    waypaper
    alacritty
    dunst
    neovim
    fastfetch
    rofi-wayland
    greetd-tuigreet
    lsd
    stow
    htop
    pavucontrol
    thunar
    grimblast
    ttf-jetbrains-mono-nerd
)
NVIDIA_WAYLAND_PACKAGES=(
    egl-wayland
    libva-nvidia-driver
)

echo "Hello! Welcome to my dotfiles installer."
echo "Prerequisites: Arch linux, yay"
echo ""

INSTALLED_NVIDIA=0
if prompt "Would you like to install NVidia (Proprietary) DKMS drivers?"; then
    install_packages "${NVIDIA_DKMS_PACKAGES[@]}"
    echo ""
    INSTALLED_NVIDIA=1
fi

if prompt "Would you like to install the default packages? (recommended)"; then
    install_packages "${DEFAULT_PACKAGES[@]}"
    if [ "$INSTALLED_NVIDIA" -eq 1 ]; then
        install_packages "${NVIDIA_WAYLAND_PACKAGES[@]}"
    fi
    echo ""
fi

if prompt "Would you like to install ZSH and Oh My Zsh?"; then
    install_packages "zsh"
    RUNZSH=no sh -c "$(curl -fsSL https://install.ohmyz.sh)"
fi

if prompt "Would you like to install all the dotfiles? (recommended)"; then
    mv $HOME/.config $HOME/.config.old
    rm -f $HOME/.zshrc
    stow . --no-folding --override='--override-all'
fi

echo "NVidia post-install instructions:"
printf "1. Add\n"
printf "     nvidia nvidia_modeset nvidia_uvm nvidia_drm\n"
printf "   to your mkinitcpio.conf, then run sudo mkinitcpio -P\n"
printf "2. Add\n"
printf "     nvidia_drm.modeset=1\n"
printf "   to your kernel parameters.\n"
printf "   GRUB: GRUB_CMDLINE_LINUX_DEFAULT in /etc/default/grub, then run sudo grub-mkconfig -o /boot/grub/grub.cfg\n"
echo ""

echo "Default post-install instructions"
echo "1. Edit /etc/greetd/config.toml and set the command setting to use tuigreet"

